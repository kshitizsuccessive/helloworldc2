const { events, Job } = require("brigadier");
events.on("push", (e,p) => {
  var job1 = new Job("job1", "docker:dind");
  job1.privileged = true;
  job1.tasks = [
    "dockerd &",
    "sleep 5",
    "cd /src",
    "ls",
    "echo $tag",
    "docker login -u $ud -p $up",
    "docker build -t $ud/dotnet2:latest .",
    "docker tag $ud/dotnet2:latest $ud/dotnet2:$tag",  
    "docker push $ud/dotnet2:$tag",
    "cd mychart/",
    "sed -i 's/.*tag.*/tag: '$(echo $tag)'/' values.yaml"
  ];

  job1.env = {
    "tag": e.revision.commit.substr(e.revision.commit.length - 7),
    "ud": p.secrets.ud,
    "up": p.secrets.up
   } 

  job1.run();
  
});
